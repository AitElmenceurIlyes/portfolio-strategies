# Portfolio strategies

In the world of finance and investment, constructing an effective portfolio is a fundamental task for investors seeking to balance risk and return. A well-constructed portfolio can help achieve financial goals and manage risk more effectively. This Python notebook explores three essential portfolio strategies: Equally Weighted, Minimum Variance, and Equal Risk Contribution. We will implement these strategies, analyze their performance, and compare their results.

In this notebook, we will focus on three common portfolio strategies:

* **Equally Weighted Portfolio:** In this strategy, all assets in the portfolio are given equal weight. This approach is straightforward and provides a balanced allocation across assets.

* **Minimum Variance Portfolio:** The goal of this strategy is to minimize the portfolio's overall volatility, i.e., the portfolio's variance or standard deviation. It seeks to find the allocation of assets that offers the lowest risk for a given set of assets.

* **Equal Risk Contribution Portfolio:** This strategy aims to distribute risk equally among the assets in the portfolio. It allocates assets in a way that each contributes an equal amount of risk to the overall portfolio.
